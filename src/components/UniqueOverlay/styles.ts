import styled from 'styled-components'
import { LogoSVG, BurgerSVG } from './IconSVG'
import { motion } from 'framer-motion'

import logo from '../../assets/logo.jpg'

export const Container = styled.div`
  position: sticky;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`

export const Header = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;

  display: flex;
  justify-content: space-between;
  align-items: center;

  padding: 0 20px;
  min-height: 52px;
`

export const Logo = styled(LogoSVG)`
  // height: 17px;
  cursor: pointer;

  height: 64px;
  width: 64px;
  margin-top: 10px;
  cursor: pointer;
  background-image: url(${logo});
  background-size: 190%;
  background-position: center;
  border-radius: 50%;
  box-shadow: 1px 1px 5px 1px black;

  path {
    display: none;
  }
`

export const Burger = styled(BurgerSVG)`
  width: 36px;
  height: 36px;
  cursor: pointer;

  padding: 10px;

  background: white;
  background-position: center;
  border-radius: 50%;
  box-shadow: 1px 1px 5px 1px black;

  svg {
    width: 16px;
  }
`

export const Footer = styled(motion.footer)`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;

  ul {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    li {
      list-style: none;

      font-size: 14px;

      & + li {
        margin: 10px 0 0;
      }

      a {
        text-decoration: none;
        color: #393c41;

        &:hover {
          color: #000;
        }
      }
    }
  }

  margin-bottom: 30px;

  @media (min-width: 600px) {
    margin-bottom: 38px;

    ul {
      flex-direction: row;

      li + li {
        margin: 0 0 0 30px;
      }
    }
  }
`
