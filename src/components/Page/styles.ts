import styled from 'styled-components'

import imgMario from '../../assets/mario.webp'
import imgSonic from '../../assets/sonic.png'
import imgCrash from '../../assets/crash.jpg'
import imgRayman from '../../assets/rayman.webp'
import imgMegaman from '../../assets/megaman.jpg'
import imgPokemon from '../../assets/pokemon.jpg'

export const Container = styled.div`
  .colored:nth-child(1) {
    background: #f1ffe7;
    background-image: url(${imgMario});
  }
  .colored:nth-child(2) {
    background: #dfffd9;
    background-image: url(${imgSonic});
  }
  .colored:nth-child(3) {
    background: #cdfeca;
    background-image: url(${imgCrash});
  }
  .colored:nth-child(4) {
    background: #bbfebb;
    background-image: url(${imgRayman});
  }
  .colored:nth-child(5) {
    background: #a9fdac;
    background-image: url(${imgMegaman});
  }
  .colored:nth-child(6) {
    background: #90f29c;
    background-image: url(${imgPokemon});
  }
  .colored:nth-child(7) {
    background: #77e68c;
    background-image: url(${imgMario});
  }

  .colored {
    background-size: cover !important;
    background-position: center !important;
  }
`

export const Spacer = styled.div`
  height: 15vh;
  background: #fff;
  box-shadow: inset 0 5px 5px 0px grey;
`
