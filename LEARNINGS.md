## Meus aprendizados com o projeto

referência: [Homepage da Tesla com ReactJS | UI Clone #14](https://www.youtube.com/watch?v=Mf4Se4ZGcG8&ab_channel=Rocketseat)
obs: no código onde estiver escrito '// Dica:' há alguma anotação sobre o conteúdo

# As novidades...

- [frame-motion](https://www.framer.com/api/motion/): Ajuda na em questão de animações
- [scroll-snap-type](https://developer.mozilla.org/en-US/docs/Web/CSS/scroll-snap-type): atributo do css que dá um efeito de 'puxar' um elemento. Algumas dicas bem legais podem ser vistas no [Blog do Willian Justen](https://willianjusten.com.br/dica-de-css-scroll-snap/), assim como uns truques do [CSS Tricks](https://css-tricks.com/practical-css-scroll-snapping/).
- custom-hooks

# Exemplos de Sites

- [FullPage](https://alvarotrigo.com/fullPage/#page1)
- [Tesla](https://www.tesla.com/)

